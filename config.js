module.exports = {
    webserver: {
        port: 3000
    },
    socket: {
        port: 8080
    },
    db: {
        url: 'mongodb://localhost/ChronoTone',
        clear_interval: 3600
    },
    git: {
        url: '/git/deploy.accept/pwd=9999517534-6999',
        current_branch: 'origin',
        branch: 'master',
        repo: 'git@bitbucket.org:defdevteam/web-app.git'
    },
    vk: {
        clientId: "5180223",
        clientSecret: "wquiCZ1PwDL6w9IAHGKl",
        callbackURL: "http://blackrocksoul.ru:3000/auth/vk/callback"
    },
    server: {
        log: './log.txt'
    }
};
