var isMobile = {
    Android: function () {
        'use strict';
        return navigator.userAgent.match(/Android/i);
    },
    BlackBerry: function () {
        'use strict';
        return navigator.userAgent.match(/BlackBerry/i);
    },
    iOS: function () {
        'use strict';
        return navigator.userAgent.match(/iPhone|iPad|iPod/i);
    },
    Opera: function () {
        'use strict';
        return navigator.userAgent.match(/Opera Mini/i);
    },
    Windows: function () {
        'use strict';
        return navigator.userAgent.match(/IEMobile/i);
    },
    any: function () {
        'use strict';
        return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows());
    }
};

function startIntro(){
    if (loading.intro) return;
    $("#settings_back").click();
    var intro = introJs();
    intro.setOptions({
        showProgress: true,
        showStepNumbers: false,
        skipLabel: 'Пропустить',
        prevLabel: 'Назад',
        nextLabel: 'Вперед',
        doneLabel: 'Завершить',
        exitOnOverlayClick: false,
        steps: [
            {
                intro: "Привет, <i>" + loading.name + "</i>!<br><br>" +
                    "Хочешь небольшую экскурсию по сайту?<br><br>" +
                    'Если считаешь, что разберешься сам, в любой момент можешь нажать "Пропустить"<br><br>' +
                    'Ну а вернуться в этот "тур", сможешь из вкладки с настройками.'
            },
            {
                element: '#playlist_search',
                intro: 'Это строка поиска. Как и ВКонтакте Вы можете найти и добавить необходимый трек.',
                position: 'bottom'
            },
            {
                element: '#menu_playlist',
                intro: 'Вот твоя основная страница с твоими аудиозаписями из ВК',
                position: 'left'
            },
            {
                element: '#chronotone',
                intro: "А как раз тут отображаются все (ну или почти все) твои песни<br><br>" +
                    "Так же на этом месте будут отображаться все треки из поиска или Ваших <i>чартов</i>",
                position: 'auto'
            },
            {
                element: '#player_control',
                intro: "Тут можно обнаружить полный набор управления плеером.<br><br>" +
                    "Ничего сложного в этом нет, но рассмотрим пару интересных вещей...",
                position: 'top'
            },
            {
                element: '#online_now',
                intro: 'Когда на этой кнопке написано "Offline", это значит, что нажав ее, Вы можете подключиться к серверу MassPlay.<br><br>' +
                    "Но об этом немного позже.<br><br>" +
                    "Как только подключишься к серверу, тут появится количество твоих слушателей. То есть тех, кто будет слышать ту музыку, какую запускаете именно ты. Ну и слева будет, соответственно, статус.",
                position: 'top'
            },
            {
                element: '#menu_settings',
                intro: 'Вкладка с настройками.<br><br>' +
                    "Тут ты можешь настраивать все, что захочешь.<br><br>" +
                    "Давай посмотрим что тут есть...",
                position: 'left'
            },
            {
                element: '#set_massPlay',
                intro: 'Настройки функции MassPlay.<br><br>' +
                    'Тут можно выбрать сервер, поставить галочку на "AutoConnect" для автоматического подключения, а так же выбрать группу, куда хочешь присоедениться.',
                position: 'left'
            },
            {
                element: '#set_acc',
                intro: 'Тут находится информация о твоем профиле.<br><br>' +
                    'На данный момент это лишь имя и звание. Звания можно получить просто за время, проведенное на этом сайте, либо одной из наших программ',
                position: 'left'
            },
            {
                element: '#set_introAgain',
                intro: 'Так как мы заканчиваем, вот тут ты можешь просмотреть весь этот "тур" с самого начала в любое время.<br><br>' +
                    'А пока, перейдем дальше...',
                position: 'left'
            },
            {
                intro: 'Последним пунктом нашего диалога на данном этапе станет небольшое послесловие...<br><br>' +
                    'Если найдешь какой-то баг, недоработку, просто пожелание по внешнему виду или функционалу, любую мелочь, не важно.. Смело пиши разработчикам, даже не задумываясь.<br>Понимаешь, нам это действительно важно. Мы тут как раз для того, чтобы делать нашу задачу именно для тебя. И мы стараемся делать это как можем, по максимуму вкладываясь в это.<br><br>Ну и конечно же.. Пользуйся в свое удовольствие! :)',
                position: 'left'
            }
        ]
    })
        .onbeforechange(function(targetElement) {
            settings = $("#menu_settings");
            switch (targetElement.id) {
                case "set_massPlay":
                case "set_acc":
                case "set_introAgain":
                    settings.click();
                    break;
                default:
                    $("#settings_back").click();
            }
        })
        .onexit(function() {
            loading.intro = true;
            $.post('/intro', {state: true});
        })
        .oncomplete(function () {
            loading.intro = true;
            $.post('/intro', {state: true});
        });
    intro.start();
}

function introAgain() {
    loading.intro = false;
    $.post('/intro');
    startIntro();
}

function loadComplete() {
    player.mainFn({
        AID: o_playlist.AID[0],
        AP: false
    });

    $('.loader_logo').animate({
        top: -$(this).height()
    }, 500);
    $('#ballsWaveG').animate({
        top: '105%'
    }, 500, function () {
        $('#ballsWaveG').remove();
        $('.block').fadeOut(500, function () {
            $('.loader_section').stop().animate({
                'width': 0
            }, 1000, function () {
                $('#loader_wrapper').remove();
            });
        });
        setTimeout(function () {
            if (!loading.intro) {
                startIntro();
            }
        }, 1500);
    });
}

function lock(text) {
    $('#lock_wrapper').show();
    $('.lock_section').animate({
        width: '53%'
    }, 1000, function () {
        $('body').contents(':not(#lock_wrapper)').remove();
        try {
            $(window.audio_stream).attr("src", "");
        } catch (error) {
        }
        o_playlist = undefined;
        o_charts = undefined;
        o_ext = undefined;
        o_search = undefined;
        $(window.audio_stream).remove();
        $('.lock_logo').animate({
            opacity: 1
        }, 500);
        $('.lock_text').text(text || 'Locked').animate({
            opacity: 1
        }, 500);
    });
}

function Loading() {
    'use strict';
    this.obj = {
        id: false,
        cookies: false,
        load: false,
        blockFixes: false,
        trackList: false,
        chartList: false,
        lazy: false
    };
    this.intro = false;
    this.elemCount = Object.keys(this.obj).length;
    this.state = 0;
    this.finish = false;

    function inc() {
        loading.state++;
        var percent = $('#load_percent');
        percent.text(Math.round(loading.state / loading.elemCount * 100) + '%');
        if (loading.state >= loading.elemCount && !loading.finish) {
            loading.finish = true;
            percent.text('Complete!');
            console.log('Load complete!');
            setTimeout(function () {
                loadComplete();
            }, 500);
        }
    }

    this.loaded = function (fn) {

        if (isMobile.any()) {
            $('.block.loader_text').text('Недосупно для мобильных устройств').css('font-size', 30);
            $('#load_percent').html('<a href="/" style="color: white;">В GooglePlay</a>');

            window.needConnect = 'false';
            window.now_connecting = true;
            $("#server_select").prop('disabled', true);
            $("#auto_conn").parent().text('Недоступно мобильных браузерах').prop({
                disabled: true,
                checked: false
            });
            setCookie('autoConn', 'false');
            console.log('В настоящее время онлайн функции не доступны для мобильных устройств. Скачайте наше приложение в Google Play');

            return;
        }

        if (!this.finish && this.obj[fn] === false) {
            this.obj[fn] = true;
            inc();
        } else {
            return;
        }

        switch (fn) {
            case "id":
                getTrackList();
                break;
            case "trackList":
                loadCharts();
                block_fixes();
                break;
            case "chartList":
                initiateLazy();
                if (window.needConnect === "true") {
                    socket.start();
                }
                break;
        }
    };
}

var audio_stream = new Audio(),
    server,
    chart,
    loading = new Loading();

function block_fixes() {
    "use strict";
    var logo = $('#logo'),
        chronoTable_right = $('.chronoTable_right'),
        search_clear = $("#search_clear"),
        playlistSearch = $("#playlist_search");
    logo.css({
        width: chronoTable_right.width(),
        left: chronoTable_right.offset().left
    }).delay(100).css({
        top: $('#chronoTable').offset().top - logo.height()
    });
    logo.on('click', function () {
        location.href = 'http://chronotone.defdev.ru';
    });

    window.scrollTo(0, 1);
    search_clear.css('top', (playlistSearch.position().top + 4));
    search_clear.css('left', playlistSearch.width() - search_clear.width() - 5);
    window.chart = $('#main_lb').attr("bl");
    loading.loaded('blockFixes');
}

$(document).ready(function () {
    "use strict";
    changeCounterFn();
});

$(window).resize(function () {
    "use strict";
    block_fixes();
});

//FUTURE СБОР НАСТРОЕК ИЗ COOKIE БРАУЗЕРА
$(document).ready(function () {
    if (getCookie('server')) {
        socket.server = getCookie('server');
        $('#server_select').val(socket.server);
    } else {
        setCookie('server', 'ws://defdev.tk');
    }
    if (getCookie('autoConn')) {
        var c = JSON.parse(getCookie('autoConn'));
        window.needConnect = getCookie('autoConn');
        $("#auto_conn").prop('checked', c);
    } else {
        setCookie('autoConn', 'false');
    }
    loading.loaded('cookies');
});
// СБОР НАСТРОЕК ИЗ COOKIE БРАУЗЕРА

//TODO Добавить время для трекбара
$(document).ready(function () {
    var trackDrag = false,
        audio = window.audio_stream;

    $(document).keyup(function (e) {
        if (!$(document.activeElement).is('input')) {
            if (e.keycode || e.which === 32) {
                player.mainFn({EVENT: 'playPause'});
                e.preventDefault();
            }
        } else {
            if (e.keycode || e.which === 32) {
                $(document.activeElement).val($(document.activeElement).val() + ' ');
            }
        }
    });

    document.documentElement.addEventListener('keydown', function (e) {
        if ((e.keycode || e.which) === 32) {
            e.preventDefault();
        }
    }, false);

    audio.addEventListener("timeupdate", function () {
        var currentTime = audio.currentTime,
            duration = audio.duration;
        if (!trackDrag) {
            $('.track_bar').stop(true, true).animate({
                'left': currentTime / duration * 100 + '%'
            }, 250, 'linear');
        }
    });

    audio.addEventListener("ended", function () {
        player.mainFn({
            EVENT: 'skip',
            SIDE: 'next'
        });
    });

    if (getCookie('volume')) {
        var x = getCookie('volume');
        $('.volumeBar').css('width', x);
        player.updateVolume(x);
    }
    audio.volume = $('.volumeBar').width() / 100;

    $('#volume').on('mousedown', function (e) {
        player.volDrag = true;
        audio.muted = false;
        $('.sound').removeClass('muted');
        updateVolume(e.pageX);
    });

    $('.track_slider').on('mousedown', function () {
        trackDrag = true;
    });
    $(document).on('mouseup', function (e) {
        if (player.volDrag) {
            player.volDrag = false;
            updateVolume(e.pageX);
        }
        if (trackDrag) {
            trackDrag = false;
            updateTrack(e.pageX);
        }
    });
    $(document).on('mousemove', function (e) {
        if (player.volDrag) {
            updateVolume(e.pageX);
        }
        if (trackDrag) {
            updateTrack(e.pageX, true);
        }
    });

    var updateVolume = function (x) {
        var volume = $('#volume'),
            percentage,
            position = x - volume.offset().left;

        percentage = 100 * position / volume.width();

        if (percentage > 99) {
            percentage = 99;
        }
        if (percentage < 0) {
            percentage = 0;
        }

        player.updateVolume(percentage);
    };

    var updateTrack = function (x, bar) {
        var track = $('.track_slider'),
            percentage,
            position = x - track.offset().left;
        percentage = 100 * position / track.width();

        if (percentage > 100) {
            percentage = 100;
        }
        if (percentage < 0) {
            percentage = 0;
        }

        $('.track_bar').css('left', percentage + '%');
        if (!bar) {
            var track_time;
            track_time = player.trackDuration / 100 * percentage;
            player.mainFn({
                EVENT: 'wind',
                tT: track_time
            });
        }
    };
});

function Playlist() {
    this.POSITION = [];
    this.AID = [];
    this.ID = [];
    this.OWNER_ID = [];
    this.ARTIST = [];
    this.TITLE = [];
    this.DURATION = [];
    this.URL = [];
    this.LYRICS_ID = [];
    this.GENRE = [];

    this.PROP = '';
    this.RAND = false;
}

Playlist.prototype.add = function (r) {
    var i;
    for (i = 0; i < r.length; i++) {
        this.POSITION.push(i);
        this.AID.push(r[i].owner_id + '_' + r[i].aid);
        this.ID.push(r[i].aid);
        this.OWNER_ID.push(r[i].owner_id);
        this.ARTIST.push(r[i].artist);
        this.TITLE.push(r[i].title);
        this.DURATION.push(r[i].duration);
        this.URL.push(r[i].url);
        this.LYRICS_ID.push(r[i].lyrics_id);
        this.GENRE.push(r[i].genre);
    }
};

//TODO добавить проверку на текущий трек. Добавлять нужные классы, если это текущий трек
Playlist.prototype.getBlock = function (aid) {
    var i = this.find(aid),
        block,
        duration,
        m,
        s;

    m = Math.floor(this.DURATION[i] / 60);
    s = this.DURATION[i] % 60;

    if (s < 10) {
        s = s.toString();
        s = "0" + s;
    }

    duration = m + ":" + s;

    block = '<div class="track" onclick="return player.mainFn({PLAYLIST: \'' + this.PROP + '\', AID:\'' + aid + '\'});">' +
        '<div class="audio_title_wrap">' +
        '<div class="artist_block">' +
        '<div class="artist" title="' + this.ARTIST[i] + '">' +
        this.ARTIST[i] +
        '</div>' +
        '</div>' +
        '<div class="playpause_btn" data-nodrag="1">' +
        '<div class="play_btn"></div>' +
        '</div>' +
        '<div class="title_block">' +
        '<div' + (this.LYRICS_ID[i] ? ' tabindex="0" class="title have_lyrics" onclick="return get_lyrics(this, ' + this.LYRICS_ID[i] + ')' : ' class="title') + '" title="' + this.TITLE[i] + '">' +
        this.TITLE[i] +
        '</div>' +
        '<div class="timer">' +
        duration +
        '</div>' +
        '<div class="add_btn_block chart_btn' + (this.PROP.match('chart.') ? ' remove_btn' : '') + '" tabindex=0 onclick="return addToChart(\'' + this.PROP + '\', \'' + aid + '\', \'' + (this.PROP.match('chart.') ? 'remove' : 'add') + '\');">' +
        '<div class="add_btn fa fa-plus-square-o"></div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        (this.LYRICS_ID[i] ? '<div class="lyrics lyrics_hiden"></div>' : '');

    return $('<div/>', {
        class: "track_block l",
        aid: this.AID[i],
        html: block
    });
};

Playlist.prototype.getTrack = function (aid) {
    var i = this.find(aid);
    return ({
        aid: this.ID[i],
        owner_id: this.OWNER_ID[i],
        artist: this.ARTIST[i],
        title: this.TITLE[i],
        duration: this.DURATION[i],
        url: this.URL[i],
        lyrics_id: this.LYRICS_ID[i],
        genre: this.GENRE[i]
    });
};

Playlist.prototype.clear = function () {
    this.POSITION = [];
    this.AID = [];
    this.ID = [];
    this.OWNER_ID = [];
    this.ARTIST = [];
    this.TITLE = [];
    this.DURATION = [];
    this.URL = [];
    this.LYRICS_ID = [];
    this.GENRE = [];
};

Playlist.prototype.find = function (aid) {
    var i,
        id = +aid.split('_')[1],
        owner_id = +aid.split('_')[0];
    for (i = 0; i < this.ID.length; i++) {
        if (this.ID[i] === id && this.OWNER_ID[i] === owner_id) {
            return i;
        } else {
            if (this.ID[i] === id) {
                return i;
            }
        }
    }
    return false;
};

Playlist.prototype.findByPos = function (num) {
    var i;
    for (i = 0; i < this.ID.length; i++) {
        if (this.POSITION[i] === num) {
            return i;
        }
    }
    return false;
};

Playlist.prototype.shuffle = function () {
    var i,
        len = this.AID.length,
        parent = $('#' + player.currentPlaylist),
        child = $(parent.children()),
        audio_row_current = $('.audio_row_current');

    if (this.RAND) {
        this.RAND = false;

        for (i = 0; i < len; i++) {
            this.POSITION.push(i);

            $("[block='" + player.currentPlaylist + "'] > [aid='" + this.AID[i] + "']").parent().insertBefore(parent.children()[i]);
        }

        if (window.audio_stream.paused) {
            player.mainFn({
                AID: $($(parent).children()[0]).children().attr("aid"),
                AP: false
            });
        } else {
            $(parent).stop(true, false).animate({
                scrollTop: $(parent).scrollTop() + audio_row_current.position().top - (window.innerHeight / 100 * 20)
            }, 500);
        }
    } else {
        this.RAND = true;
        while (child.length) {
            parent.append(child.splice(Math.floor(Math.random() * child.length), 1)[0]).index();
        }

        if (!window.audio_stream.paused && $(audio_row_current[0]).index() !== 0) {
            $(audio_row_current[0]).insertBefore($(audio_row_current[0]).parent().children()[0]);
            //            player.currentTrack = 0;
        } else {
            player.mainFn({
                AID: $($(parent).children()[0]).children().attr("aid"),
                AP: false
            });
        }

        for (i = 0; i < len; i++) {
            var current_aid = $("[block='" + player.currentPlaylist + "'] > [aid='" + this.AID[i] + "']");
            if (this.POSITION[current_aid.parent().index()] === undefined) {
                console.log('undef');
            }
            this.POSITION.push(this.POSITION[current_aid.parent().index()]);
        }
    }

    this.POSITION.splice(0, len);
    $('#chronotone').children('#' + player.currentPlaylist).scroll();

};

function Player() {
    this.currentTrack = -1;
    this.currentPlaylist = 'playlist';
    this.state = 'pause';
    this.repeat = false;
    this.trackDuration = 0;
    this.trackDrag = false;
    this.volume = 50;
    this.volDrag = false;

    this.mainFn = function (fn) {
        if (window.text_clicked) {
            window.text_clicked = false;
            return;
        }

        if (socket.open && socket.role !== 'Owner') {
            return;
        }

        if (!fn.EVENT) {
            fn.EVENT = "play";
        }

        if (!fn.PLAYLIST) {
            fn.PLAYLIST = player.currentPlaylist;
        }

        fn.O_PL = this.findPlaylist(fn.PLAYLIST);

        if (!fn.AID) {
            fn.AID = fn.O_PL.AID[player.currentTrack];
        }

        fn.BLOCK = $('#' + fn.PLAYLIST).find('[aid="' + fn.AID + '"]').parent();

        switch (fn.EVENT) {
            case "playPause":
                fn.i = fn.O_PL.find(fn.AID);
                if (socket.open) {
                    fn.EVENT = 'play';
                    delete fn.i;
                    socket.send(fn);
                    return;
                }
                player.play(fn);
                break;
            case "play":
                fn.i = fn.O_PL.find(fn.AID);
                if (player.currentTrack === fn.i && player.currentPlaylist === fn.PLAYLIST) {
                    if (this.state === 'play') {
                        player.mainFn({
                            EVENT: 'pause'
                        });
                        return;
                    }
                }
                if (socket.open) {
                    fn.AP = fn.AP === undefined ? true : fn.AP;
                    if (player.currentTrack === fn.i) {
                        fn.TRACKTIME = window.audio_stream.currentTime;
                    } else {
                        fn.TRACKTIME = 0;
                    }
                    socket.send(fn);
                    return;
                }
                player.play(fn);
                break;
            case "pause":
                if (socket.open) {
                    fn.TRACKTIME = window.audio_stream.currentTime;
                    socket.send(fn);
                    return;
                }
                player.pause();
                break;
            case "skip":
                player.skip(fn.SIDE, fn.O_PL);
                break;
            case "shuffle":
                fn.O_PL.shuffle();
                break;
            case "wind":
                player.wind(fn.tT);
                break;
            case "rewind":
                if (socket.open) {
                    delete fn.PLAYLIST;
                    fn.ST = !window.audio_stream.loop;
                    socket.send(fn);
                    return;
                }
                player.rewind(!window.audio_stream.loop);
                break;
        }
    };

    this.findPlaylist = function (playlist) {
        switch (playlist) {
            case "playlist":
                return o_playlist;
            case "search_block":
                return o_search;
            case "ext":
                return o_ext;
            case playlist.match('chart.')[0]:
                return o_charts[playlist.match('chart.')[0].charAt((playlist.match('chart.')[0]).length - 1) - 1];
        }
    };

    this.play = function (fn) {
        var O_PL = fn.O_PL || this.findPlaylist(fn.PLAYLIST),
            i = (fn.i !== undefined) ? fn.i : fn.O_PL.find(fn.AID),
            block = fn.BLOCK || $('#' + fn.PLAYLIST).find('[aid="' + fn.AID + '"]').parent(),
            AP = fn.AP;

        if (player.currentTrack === i && player.currentPlaylist === fn.PLAYLIST && AP === undefined) {
            switch (player.state) {
                case "play":
                    player.pause();
                    return;
                case "pause":
                    $("#btn_play").addClass("hide");
                    $("#btn_pause").removeClass("hide");
                    $('.now_playing').removeClass('now_playing');
                    $(fn.BLOCK).find('.play_btn').addClass('now_playing');
                    window.audio_stream.play();
                    player.state = 'play';
                    return;
            }
        }

        if (player.currentTrack !== i || player.currentPlaylist !== fn.PLAYLIST || fn.PLAYLIST === 'ext') {
            window.audio_stream.currentTime = 0;
            window.audio_stream.src = O_PL.URL[i];
            window.audio_stream.duration = O_PL.DURATION[i];
        }

        if (fn.tT) { //TODO поправить тректайм
            window.audio_stream.currentTime = fn.tT;
        } else {
            if (player.state !== 'pause' && player.currentTrack !== i) {
                window.audio_stream.currentTime = 0;
            }
        }

        if (AP !== false) {
            $("#btn_play").addClass("hide");
            $("#btn_pause").removeClass("hide");
            $('.now_playing').removeClass('now_playing');
            $(block).find('.play_btn').addClass('now_playing');
            player.state = 'play';
            window.audio_stream.autoplay = true;
            if (window.audio_stream.paused) {
                window.audio_stream.play();
            }
        } else {
            $("#btn_play").removeClass("hide");
            $("#btn_pause").addClass("hide");
            $('.now_playing').removeClass('now_playing');
            player.state = 'pause';
            window.audio_stream.autoplay = false;
            window.audio_stream.pause();
        }

        if (player.currentTrack !== i || player.currentPlaylist !== fn.PLAYLIST) {
            $('.audio_row_current').removeClass('audio_row_current');
            $(block).addClass('audio_row_current');
        }

        this.currentTrack = i;
        this.currentPlaylist = fn.PLAYLIST;
        this.trackDuration = O_PL.DURATION[i];

        this.setTitle({
            O_PL: O_PL,
            i: i
        });

        if (fn.SIDE && activeBlock.active === fn.PLAYLIST) {
            try {
                $("#" + fn.PLAYLIST).stop(true, false).animate({
                    //TODO сделать проверку на текущий плейлист.
                    //При переходе в другое плейлист выбивает ошибку
                    scrollTop: $("#" + fn.PLAYLIST).scrollTop() + $('.audio_row_current').position().top - (window.innerHeight / 100 * 20)
                }, 500);
            } catch (nothing) {}
        }
    };

    this.pause = function () {
        window.audio_stream.pause();
        player.state = 'pause';
        $('.now_playing').removeClass('now_playing');
        $("#btn_play").removeClass("hide");
        $("#btn_pause").addClass("hide");
    };

    this.skip = function (side, playlist) {
        if (side === 'prev') {
            if (playlist.RAND) {
                if (playlist.AID[playlist.findByPos(playlist.POSITION[player.currentTrack] - 1)]) {
                    player.mainFn({
                        EVENT: 'play',
                        SIDE: side,
                        AID: playlist.AID[playlist.findByPos(playlist.POSITION[player.currentTrack] - 1)],
                        AP: true
                    });
                } else {
                    player.mainFn({
                        EVENT: 'play',
                        SIDE: side,
                        AID: playlist.AID[playlist.findByPos(playlist.AID.length - 1)],
                        AP: true
                    });
                }
            } else {
                if (playlist.AID[player.currentTrack - 1]) {
                    player.mainFn({
                        EVENT: 'play',
                        SIDE: side,
                        AID: playlist.AID[player.currentTrack - 1],
                        AP: true
                    });
                } else {
                    player.mainFn({
                        EVENT: 'play',
                        SIDE: side,
                        AID: playlist.AID[playlist.AID.length - 1],
                        AP: true
                    });
                }
            }
        }

        if (side === 'next') {
            if (playlist.RAND) {
                if (playlist.AID[playlist.findByPos(playlist.POSITION[player.currentTrack] + 1)]) {
                    player.mainFn({
                        EVENT: 'play',
                        SIDE: side,
                        AID: playlist.AID[playlist.findByPos(playlist.POSITION[player.currentTrack] + 1)],
                        AP: true
                    });
                } else {
                    player.mainFn({
                        EVENT: 'play',
                        SIDE: side,
                        AID: playlist.AID[playlist.findByPos(0)],
                        AP: true
                    });
                }
            } else {
                if (playlist.AID[player.currentTrack + 1]) {
                    player.mainFn({
                        EVENT: 'play',
                        SIDE: side,
                        AID: playlist.AID[player.currentTrack + 1],
                        AP: true
                    });
                } else {
                    player.mainFn({
                        EVENT: 'play',
                        SIDE: side,
                        AID: playlist.AID[0],
                        AP: true
                    });
                }
            }
        }
    };

    this.wind = function (tT) {
        window.audio_stream.currentTime = tT;
    };

    this.rewind = function (status) {
        if (!status) {
            this.repeat = false;
            window.audio_stream.loop = false;
            $("#btn_loop").attr('src', '/images/player/rewind_btn_sm.png');
        } else {
            this.repeat = true;
            window.audio_stream.loop = true;
            $("#btn_loop").attr('src', '/images/player/rewind_a_btn_sm.png');
        }
    };

    this.setTitle = function (fn) {
        var artist = fn.O_PL.ARTIST[fn.i].replace(/&amp;/g, '&'),
            title = fn.O_PL.TITLE[fn.i].replace(/&amp;/g, '&'),
            artist_sl = artist.slice(0, 50),
            title_sl = title.slice(0, 60);

        window.audio_stream.title = artist + ' - ' + title;

        if (artist_sl.length < artist.length) {
            artist_sl += '...';
        }

        if (title_sl.length < title.length) {
            title_sl += '...';
        }

        $("#head_author").text(artist_sl).attr('title', artist);
        $("#head_title").text(title_sl).attr('title', title);
    };

    this.updateVolume = function (val) {
        var audio = window.audio_stream,
            calc;

        if (val > 99) {
            val = 99;
        }
        if (val < 0) {
            val = 0;
        }

        calc = ((Math.pow(Math.exp(1), (val / 100))) - 1) / 1.69123447235;

        $('.volumeBar').css('width', val + '%');

        player.volume = val;
        audio.volume = calc;

        if (!this.volDrag) {
            setCookie('volume', val);
        }

        if (audio.volume === 0) {
            $('#mute_btn').addClass('muted');
        } else if (audio.volume > 0) {
            $('#mute_btn').removeClass('muted');
        }

    };

    this.mute = function () {
        var calc;
        if (window.audio_stream.volume === 0) {
            if (this.volume === 0) {
                this.volume = 10;
                setCookie('volume', '10');
            }
            $('.volumeBar').css('width', this.volume + '%');
            calc = ((Math.pow(Math.exp(1), (this.volume / 100))) - 1) / 1.69123447235;
            window.audio_stream.volume = calc;
            $("#mute_btn").removeClass("muted");
        } else {
            $('.volumeBar').css('width', '0%');
            window.audio_stream.volume = 0;
            $("#mute_btn").addClass("muted");
        }
    };
}

var o_playlist = new Playlist(),
    o_search = new Playlist(),
    o_charts = [],
    player = new Player();

//TODO Добавить действие "Обновить" на кнопку "ChronoTone"
//TODO Исправить порядок добавления кнопок

function createChartBlocks(name) {
    $('<div/>', {
        id: 'chart' + name,
        class: 'main_block'
    }).appendTo('#chronotone');

    $('<div/>', {
        id: 'menu_chart' + name,
        class: 'menu_item chart',
        onclick: "activeBlock.show(this, 'chart" + name + "')",
        text: 'Чарт ' + name
    }).insertBefore('.settings_block');

    // $(function () {
    //     $("#chart" + name).sortable();
    //     $("#chart" + name).disableSelection();
    // });

    o_charts[name - 1] = new Playlist();
}

$.ajaxSetup({
    cache: false
});

function loadCharts() {
    $.post('/db/playlist', function (data) {
        if (data) {
            var i;
            try {
                var allSongs = '',
                    songCounter = [];

                for (i = 0; i < data.length; i++) {
                    if (data[i].trackList.length) {

                        createChartBlocks(i + 1);

                        songCounter[i] = +data[i].amountTracks;
                        allSongs += data[i].trackList;
                        if (i !== data.length - 1 && allSongs.length) {
                            allSongs += ',';
                        }
                    } else {
                        return loading.loaded("chartList");
                    }
                }
                if (allSongs.length) {
                    getChartList(allSongs, songCounter);
                }

            } catch (err) {
                // setTimeout(function () {
                //     loadCharts();
                // }, 1000);
                console.error(err.message);
            }
        } else {
            loading.loaded("chartList");
            $(".chart_tabs > label").hide();
            $(".chart_tabs > section").css('border', 'none');
        }
    });
}

function addToChart(playlist, aid, want, pl_name) {
    window.text_clicked = true;
    if (!pl_name) {
        pl_name = 1;
    }

    if (want === 'add' && $('#chart' + pl_name).children().find('[aid=' + aid + ']').length) {
        return;
    }

    var block = $('[block="' + playlist + '"] ' + '[aid=' + aid + ']');

    $.post('/db/playlist_' + want, {
        pl_name: pl_name,
        track: aid,
        want: want
    }, function () {
        if (want === 'add') {
            if (!$('#menu_chart' + pl_name).length) {
                createChartBlocks(pl_name);
            }

            block.clone().attr({
                id: 'clone',
                onclick: ''
            }).insertAfter($('body')).css({
                position: 'fixed',
                "pointer-events": 'none',
                width: block.width()
            }).offset(block.offset()).animate({
                left: $($('#menu_chart' + pl_name)).offset().left,
                top: $($('#menu_chart' + pl_name)).offset().top,
                width: $($('#menu_chart' + pl_name)).width()
            }, {
                step: function () {
                    $(this).css('-webkit-transform', "scale(0.5)");
                },
                duration: 'slow'
            }, 'linear').fadeOut(300, function () {
                $(this).remove();
            });

            o_charts[pl_name - 1].add([player.findPlaylist(playlist).getTrack(aid)]);
            o_charts[pl_name - 1].POSITION[o_charts[pl_name - 1].POSITION.length - 1] = o_charts[pl_name - 1].POSITION.length - 1;

            block.parent().clone().removeClass('audio_row_current').attr({
                block: 'chart' + pl_name
            }).appendTo('#chart' + pl_name).find('.track').attr({
                onclick: "return player.mainFn({PLAYLIST: 'chart" + pl_name + "', AID:'" + aid + "'});"
            }).find('.chart_btn').attr({
                onclick: "addToChart('chart" + pl_name + "', '" + aid + "', 'remove');"
            }).addClass('remove_btn').parents('.track_block').find('.now_playing').removeClass('now_playing');
        }

        //TODO Исправить удаление для других чартов кроме первого через $.Parents()

        if (want === 'remove') {
            $(block).fadeOut(200, function () {
                $(this).parent().remove();
                if (!$('#chart' + pl_name).children().length) {
                    activeBlock.hide();
                    $('#chart' + pl_name).remove();
                    $('#menu_chart' + pl_name).remove();
                }
            });
        }
    });
}

$(window).on('load', function () {
    loading.loaded("load");
});

function initiateLazy() {
    $("[l='l']").Lazy({
        scrollDirection: 'vertical',
        threshold: 1000,
        autoDestroy: true,
        visibleOnly: true,
        appendScroll: $("#chronotone").children(),
        onError: function (element) {
            element.html('error');
        },
        lz: function (element, response) {
            setTimeout(function () {
                var id = element.children().attr('aid'),
                    block = element.attr('block'),
                    object;
                switch (block) {
                    case "playlist":
                        object = o_playlist;
                        break;
                    case "search_block":
                        object = o_search;
                        break;
                    case block.match('chart.')[0]:
                        object = o_charts[block.match('chart.')[0].charAt((block.match('chart.')[0]).length - 1) - 1];
                        break;
                }
                element.html(object.getBlock(id));
                element.removeAttr('aid');
                response(true);
            }, 500);
        }
    });
    loading.loaded("lazy");
}

function search(val) {
    if (val.length !== 0) {
        getSearchList(val);

        if ($('#menu_search').css('display') === 'none') {
            $('#menu_search').fadeIn(100);
        }

        if (activeBlock.active !== 'search_block') {
            activeBlock.show($('#menu_search'), 'search_block');
        }

    } else {
        activeBlock.hide();
    }
}

function ActiveBlock() {
    this.main = "playlist";
    this.active = "playlist";

    this.show = function (menu, id) {
        if (id === this.active && id === this.main) {
            return;
        }

        if (id !== this.active) {
            $('#' + this.active).stop().fadeOut(200);
            $('#' + id).stop().fadeIn(200);
            $('.active').removeClass('active');
            $(menu).addClass('active');
            $('#chronotone').children().scroll();
            this.active = id;
        } else {
            $('#' + this.active).stop().fadeOut(200);
            $('#' + this.main).stop().fadeIn(200);
            $('.active').removeClass('active');
            $('#menu_' + this.main).addClass('active');
            this.active = this.main;
        }
    };

    this.hide = function () {
        $('#' + this.active).stop().fadeOut(200);
        $('#' + this.main).stop().fadeIn(200);
        $('.active').removeClass('active');
        $('#menu_' + this.main).addClass('active');
        this.active = this.main;
    };
}

var activeBlock = new ActiveBlock();

function setServer(list) {
    window.server = list.value;
    setCookie('server', list.value);
    //noinspection JSUnusedGlobalSymbols,JSUnusedGlobalSymbols
    alertBox("Обновить", "Чтобы настройки вступили в силу, необходимо обновить страницу", true, {
        Позже: function () {
            $(this).dialog('destroy');
            $(this).remove();
        },
        Перезагрузить: function () {
            $(this).dialog('destroy');
            location.reload();
            $(this).remove();
        }
    });
}

function autoConn() {
    var s = $("#auto_conn").prop('checked').toString();
    window.needConnect = s;
    setCookie('autoConn', s);
    //noinspection JSUnusedGlobalSymbols,JSUnusedGlobalSymbols
    alertBox("Обновить", "Чтобы настройки вступили в силу, необходимо обновить страницу", true, {
        Позже: function () {
            $(this).dialog('destroy');
            $(this).remove();
        },
        Перезагрузить: function () {
            $(this).dialog('destroy');
            $(this).remove();
            location.reload();
        }
    });
}

function get_lyrics(obj, id) {
    var block;
    window.text_clicked = true;
    block = $(obj).parent().parent().parent().parent().children('.lyrics');

    if ($(block).attr('class').indexOf('hiden') != -1) {
        $(block).removeClass('lyrics_hiden');
        if ($(block).text() !== undefined && id !== undefined) {
            VK.Api.call('audio.getLyrics', {
                lyrics_id: id
            }, function (s) {
                $(block).text(s.response.text);
            });
        }
    } else {
        $(block).addClass('lyrics_hiden');
    }
}

function updateUserInfo(login, partyId, statusCode) {
    var status;
    switch (statusCode) {
        case 951753:
            status = 'Няшка админ <i class="fa fa-male" style="color: deepskyblue;font-size: 20px;" aria-hidden="true"></i>';
            break;
        case 888888:
            status = 'Участник ЗБТ';
            break;
        default:
            status = 'Проходимец';
            break;
    }

    $('#partyId').text(partyId);
    $('#settings_name').append(login);
    $('#settings_status').append(status);
}

function setUserCounter(role, counter) {
    socket.online = counter;
    $("#user_counter").text(socket.online);

    switch (role) {
        case 0:
            socket.role = 'Owner';
            break;
        case 1:
            socket.role = 'User';
            break;
        case false:
            socket.role = 'Offline';
            break;
    }

    if (socket.role === 'Offline') {
        $("#online_role").fadeOut('fast');
    } else {
        $("#online_role").text(socket.role);
        $("#online_role").fadeIn('fast');
    }

    changeCounterFn();
}

function changeCounterFn() {
    if (socket.role === 'Offline') {
        $('#online_now').css('cursor', 'pointer');
        $('#online_now').attr('title', 'Подключиться к серверу');
    } else {
        $('#online_now').css('cursor', 'default');
        $('#online_now').attr('title', 'Слушателей онлайн: ' + socket.online);
    }
}

function mask(input, format, sep) {
    var output = "";
    var idx = 0;
    for (var i = 0; i < format.length && idx < input.length; i++) {
        output += input.substr(idx, format[i]);
        if (idx + format[i] < input.length) output += sep;
        idx += format[i];
    }

    output += input.substr(idx);

    return output;
}

$('#conn_partyId').keyup(function() {
    var foo = $(this).val().replace(/-/g, "");
    if (foo.length > 0) {
        foo = mask(foo, [3], "-");
    }
    $(this).val(foo);
});

function selectText(block) {
    var range;
    if (document.selection) {
        range = document.body.createTextRange();
        range.moveToElementText(block);
        range.select();
    } else if (window.getSelection) {
        range = document.createRange();
        range.selectNode(block);
        window.getSelection().addRange(range);
    }
}