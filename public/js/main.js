var attempt;

/**
 * Получить куки
 * @param   {string} name - имя куки
 * @returns {string} возвращает значение куки
 */
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function alertBox(title, text, modal, buttons) {
    jQuery('<div/>', {
        id: 'dialog-message',
        title: title,
        html: '<p>' + text + '</p>'
    }).appendTo('body').dialog({
        modal: modal,
        closeOnEscape: false,
        width: window.innerWidth / 100 * 35,
        buttons: buttons
        //        {
        //            Ok: function() {
        //                $( this ).dialog( "close" );
        //            }
        //        }
    });
}

/**
 * Задать куки
 * @param {string} name        - имя куки
 * @param {string} value       - значение куки
 * @param {object} [options={] - опции
 */
function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires,
        propName,
        d = new Date();

    if (typeof expires === "number" && expires) {
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }

    document.cookie = updatedCookie;
}

$(document).ready(function () {
    "use strict";
    VK.init({
        apiId: 5180223
    });
    VK.Auth.getLoginStatus(authInfo);
});

//TODO Сделать круто
function returnUser(err, first) {
    if (first !== false) {
        first = true;
    }
    switch (err) {
        case 'auth':
            // alert("Вы не авторизованы и не можете тут находиться.");
            location.href = "/join";
            break;
        case '':
            alert("К сожалению, у Вас не хватает прав для этого.\nСкорее всего Ваш аккаунт был заблокирован\nОбратитесь к администраторам, они хорошие люди :).");
            location.href = "/";
            break;
        case 951753:
            if (first) devConsole('Доброго времени суток, Администратор. Опять что-то сломалось? :)');
            break;
        case 888888:
            if (first) console.log('Доброго времени суток, друг! Вы находитесь в закрытом бета тестировании. При любых ошибках, сообщайте администратору');
            break;
        case 111111:
            alert("К сожалению, сейчас идет закрытый тест и Вы не входите в группу разработчиков.\nЕсли у вас есть предложения, обратитесь к администраторам.");
            location.href = "/";
            break;
        default:
            alert("Ошибка!\nОбратитесь к администраторам");
            location.href = "/";
    }

}

$.ajaxSetup({
    cache: false
});

function authInfo(response) {
    "use strict";
    var data = {},
        user = {};
    if (response.status === 'connected') {
        data = response.session;
        window.userId = parseInt(response.session.mid);
        VK.Api.call('users.get', {fields: 'sex'}, function (res) {
            if (res.response) {
                user = res.response[0];
                data.user = user;
                var fullname = res.response[0].first_name + res.response[0].last_name,
                    name = res.response[0].first_name;
                $.ajax({
                    url: '/auth',
                    method: 'POST',
                    data: data,
                    dataType: 'JSON',
                    success: function (res) {
                        if (res) {
                            window.partyId = res.partyId;
                            window.user_access = res.password;
                            devConsole('Ваш уровень доступа: ' + window.user_access);
                            if (location.href.match("player")) {
                                window.returnUser(window.user_access);
                                loading.loaded("id");
                                loading.intro = res.intro;
                                loading.name = name;
                                updateUserInfo(fullname, window.partyId, res.password);
                            } else {
                                if (location.href.match("join")) {
                                    location.reload();
                                }
                            }
                        } else {
                            alert("Ошибка при регистрации, обратитесь к администраторам.\nК сожалению некоторые функции до сих пор могут иметь некоторые ошибки.");
                            location.href = "/";
                        }
                    }
                });
            }
        })
    } else {
        $.post('/logout');
        if (location.href.match("player")) {
            window.returnUser('auth');
        }
    }
}

function devConsole(obj) {
    if (window.user_access == 951753) {
        if (typeof obj == 'string') {
            console.log("Dev Console: " + obj);
        } else {
            var z = $(obj)[0],
                s = '<Dev_Console>' + $(z).html();
            console.log($(s)[0]);
        }
    }
}