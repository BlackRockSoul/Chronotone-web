var socket,
    ready = false,
    role = 'Owner',
    online = 'Offline',
    now_connecting = false;

function Socket() {
    this.open = false;
    this.state = 'closed';
    this.server = '';
    this.role = 'Offline';
    this.online = 0;
    this.socketStream;

    this.start = function () {
        if (this.open === true) return;
        this.state = 'Connecting';
        this.socketStream = new WebSocket(this.server);
        setUserCounter(false, 'Loading');

        this.socketStream.onopen = function () {
            socket.open = true;
            socket.state = 'Open';
            var packet = {
                "PROTOCOL": "1A",
                "USERID": window.userId,
                "PARTYID": window.partyId
            };
            this.send(JSON.stringify(packet));
        };

        this.socketStream.onclose = function (event) {
            setUserCounter(false, 'Offline');

            if (socket.state === 'Restart') {
                alert('Рестарт сервера.\nАвтоматическое переподключение через 10 секунд...');
                alertBox("Рестарт", "Рестарт сервера.\nАвтоматическое переподключение через 10 секунд...", true, {
                    Ок: function () {
                        $(this).dialog('destroy');
                        $(this).remove();
                    }
                });
                setTimeout("socket.start();", 10000);
                return;
            }

            socket.open = false;
            socket.state = 'Closed';

            if (event.wasClean) {
                devConsole('Sockets: Соединение закрыто чисто');
            } else {
                alertBox("ОШИБКА", "Ошибка соединения с сервером: " + event.reason + " Попробуйте обновить страницу, либо обратитесь к администрации", true, {
                    Ок: function () {
                        $(this).dialog('destroy');
                        $(this).remove();
                    }
                });
                console.error('Sockets: Обрыв соединения');
                console.error('Sockets: Код: ' + event.code + ' причина: ' + event.reason);
            }
        };

        this.socketStream.onerror = function (error) {
            console.error("Sockets: Ошибка " + error.message);
        };

        this.socketStream.onmessage = function (event) {
            var query = JSON.parse(event.data);
            console.log('%c SERVER: ' + event.data, 'background: orange; color: black');

            switch (query.EVENT) {
            case "play":
                getExtTrack(query);
                break;
            case "pause":
                player.pause();
                break;
            case "wind":
                player.wind(query.TT + (getTime() - query.TIME));
                break;
            case "rewind":
                player.rewind(query.ST);
                break;
            case "shuffle":
                shuffle(query.AP);
                break;
            case "bad_protocol":
                alert("Ошибка: Bad Protocol \nОшибка с подключением к серверу. Проверка протокола клиента: неудача \nУбедительная просьюа обратиться к администраторам");
                break;
            case "SERVICE":
                setUserCounter(query.ROLE, query.ONLINE);
                break;
            case "RESTART":
                socket.state = 'Restart';
                break;
            case "":
                socket.socketStream.close()
            }

        };

    };

    this.send = function (fn) {
        if (this.open === false || this.role !== 'Owner') {
            return;
        };

        var query = {
            EVENT: fn.EVENT,
            TIME: getTime(),
            AID: fn.AID,
            PLAYLIST: fn.PLAYLIST,
            TRACKTIME: fn.TRACKTIME,
            DURATION: fn.DURATION,
            AP: fn.AP
        };

        query = JSON.stringify(query);
        console.log('%c CLIENT: ' + query, 'background: purple; color: white');
        this.socketStream.send(query);
    };

}

function ExtPlaylist() {
    this.AID = [];
    this.ID = [];
    this.OWNER_ID = [];
    this.ARTIST = [];
    this.TITLE = [];
    this.DURATION = [];
    this.URL = [];
    this.LYRICS_ID = [];
    this.GENRE = [];

    this.AP = false;
    this.TRACKTIME = 0;
    this.TIME = 0;

    this.add = function (r) {
        var i = 0;
        this.AID = [r[i].owner_id + '_' + r[i].aid];
        this.ID = [r[i].aid];
        this.OWNER_ID = [r[i].owner_id];
        this.ARTIST = [r[i].artist];
        this.TITLE = [r[i].title];
        this.DURATION = [r[i].duration];
        this.URL = [r[i].url];
        this.LYRICS_ID = [r[i].lyrics_id];
        this.GENRE = [r[i].genre];

        this.play();
    };

    this.play = function () {
        
        player.play({
            EVENT: 'play',
            PLAYLIST: 'ext',
            AID: this.AID,
            AP: this.AP,
            i: 0,
            BLOCK: 'online'
        });
        
        

        if (this.AP === true) {
            window.audio_stream.onloadeddata = function () {
                window.audio_stream.play();
                if (socket.online > 0) {
                    var setTime = (getTime() - (o_ext.TIME - timeOffset));
                    window.audio_stream.currentTime = setTime + o_ext.TRACKTIME;
                    console.log(setTime);
                }
            }
        }
    };
};

var socket = new Socket(),
    o_ext = new ExtPlaylist();

window.onbeforeunload = function () {
    if (socket.open) {    
        socket.socketStream.onclose = function () {};
        socket.socketStream.send(JSON.stringify({
            EVENT:"DISCONNECT",
            REASON:"Unload"
        }));
        socket.socketStream.close();
        socket.open = false;
        socket.state = 'Closed';
    }
};

function getTime() {
    if (this.open) {
        var s = new Date(),
            timeNow = s.getTime() / 1000;
        return timeNow + timeOffset;
    } else {
        return 0;
    }
};

function upperCaseKeys(o) {
    var r = {};
    for (var p in o)
        r[p.toUpperCase()] = o[p];
    return r;
};

function lowerCaseKeys(o) {
    var r = {};
    for (var p in o)
        r[p.toLowerCase()] = o[p];
    return r;
};