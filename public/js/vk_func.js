function getTrackList() {
    VK.Api.call('audio.get', {}, function (r) {
        if (r.error) {
         return console.error('VK audio.get:', r.error.error_msg);
        } else {
//            errorCounter.clear();
        }
        
        var i,
            response = r.response,
            aid,
            lazyBlock;
        
        //response.splice(0, 1);
        
        o_playlist.PROP = 'playlist';
        o_playlist.add(response);
        
        for (i = 0; i < r.response.length; i++) {
            
            aid = response[i].owner_id + '_' + response[i].aid
            
            lazyBlock = $(lazyBlock).add($('<div/>', {
                block: 'playlist',
                "data-loader": "lz",
                l: "l",
                html: '<div aid=' + aid + ' style="width=100%;height:42px" onclick="player.mainFn({PLAYLIST: \'playlist\', AID:\'' + aid + '\'});"></div>'
            }));
        }
        
        lazyBlock.appendTo('#playlist');
        loading.loaded("trackList");
        
    });
}

function getChartList(trackList, counter) {
    VK.Api.call('audio.getById', {
        audios: trackList
    }, function (r) {
        if (r.error) {
            return console.error('VK audio.getById:', r.error.error_msg);
        } else {
            //            errorCounter.clear();
        }
        
        var i,
            q,
            response = r.response,
            aid,
            slice,
            x = 0,
            y = counter[0];
        
        for(i = 0; i < counter.length; i++) {
            slice = r.response.slice(x, y);
            o_charts[i].add(slice);
            o_charts[i].PROP = "chart" + (i + 1);
            x = y;
            y += counter[i+1];
            createLazy(slice, "chart" + (i + 1));
        }

        function createLazy (response, block) {
            for (q = 0; q < response.length; q++) {

                aid = response[q].owner_id + '_' + response[q].aid;

                $('<div/>', {
                    block: block,
                    "data-loader": "lz",
                    l: "l",
                    html: '<div aid=' + aid + ' style="width=100%;height:42px" onclick="player.mainFn({PLAYLIST: \'' + block + '\', AID:\'' + aid + '\'});"></div>'
                }).appendTo('#' + block);
            }
        };        
        
        loading.loaded("chartList");
    });                
}

function getSearchList(val) {
    VK.Api.call('audio.search', {
        q: val,
        search_own: 1,
        count: 200
    }, function (r) {
        if (r.error) {
            return console.error('VK audio.search:', r.error.error_msg);
        } else {
            //            errorCounter.clear();
        }

        var i,
            response = r.response,
            aid,
            lazyBlock;

        response.splice(0, 1);
        
        o_search.clear();
        o_search.PROP = 'search_block';
        o_search.add(response);

        for (i = 0; i < response.length; i++) {

            aid = response[i].owner_id + '_' + response[i].aid;

            lazyBlock = $(lazyBlock).add($('<div/>', {
                block: 'search_block',
                "data-loader": "lz",
                l: "l",
                html: '<div aid=' + aid + ' style="width=100%;height:42px" onclick="player.mainFn({PLAYLIST: \'search_block\', AID:\'' + aid + '\'});"></div>'
            }));
        }

        $('#search_block').children().remove();
        lazyBlock.appendTo('#search_block');
        initiateLazy()
        
    });
}

function getExtTrack(query) {
    var aid = query.AID;
    
    if (socket.role === "Owner") {
        var o_pl = player.findPlaylist(query.PLAYLIST);
        query.i = o_pl.find(aid);
        player.play(query);
        return;
    }
    
    VK.Api.call('audio.getById', {
        audios: aid
    }, function (r) {
        if (r.error) {
            return console.error('VK audio.getById. Ext track:', r.error.error_msg);
        } else {
            //            errorCounter.clear();
        }
        
        o_ext.TIME = query.TIME;
        o_ext.TRACKTIME = query.TRACKTIME;
        o_ext.AP = query.AP;
        o_ext.add(r.response);
        
    });                
}