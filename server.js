require("./app/dev/logger");

var app = require(__dirname + '/app/main'),
    cluster  = require('cluster'),
    config = require('./config.js'),
    moment = require('moment');

global.__base = __dirname + '/';

var l_webserver = global.winston.loggers.get('webserver'),
    l_worker = global.winston.loggers.get('worker'),
    l_proxy = global.winston.loggers.get('proxy');


var port = config.webserver.port;

if (cluster.isMaster) {

    require('./app/dev/console');
    require('./app/dev/restart');

    var numberOfCPUs = require('os').cpus().length;

    if(app) l_webserver.info('Started on port', port, 'in', numberOfCPUs, 'threads');

    for (var i = 0; i < numberOfCPUs; i++) {
        cluster.fork();
    }

    cluster.on('disconnect', function(worker) {
        l_worker.warn(worker.id, 'disconnect');
    });
    cluster.on('exit', function(worker, code, signal) {
        l_worker.error(color.red('[WORKER]'), '  ', worker.id, 'die', signal || code);
        if (!worker.suicide && !global.stop) {
            l_worker.info('Starting new Worker', worker.id);
            cluster.fork();
        }
    });
}

if (cluster.isWorker) {
    var http = require('http'),
        httpProxy = require('http-proxy'),
        proxy = httpProxy.createProxyServer({}),
        url = require('url');

    http.createServer(function(req, res) {
        var hostname = req.headers.host.split(":")[0];

        switch(hostname)
        {
            case '127.0.0.1':
                proxy.web(req, res, { target: 'http://defdev.ru:80' });
                break;
            default:
                proxy.web(req, res, { target: 'http://localhost:3000' });
        }
    }).listen(80, function() {
        l_proxy.info('Listening on port 80');
    });

    app.listen(port).on('error', function(err) {
        l_webserver.error(color.red(err.message));
    });
}