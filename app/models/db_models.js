module.exports = {
    client: {
        id: {type: Number, default: 0, unique: true},
        partyId: {type: String},
        login: {type: String},
        password: {type: Number, default: 111111},
        intro: {type: Boolean},
        date: {type: Date}
    },
    playlist: {
        id: {type: Number, default: 0},
        playlistName: {type: Number, default: 0.1, unique: true},
        name: {type: String},
        amountTracks: {type: Number, default: 0},
        trackList: {type: Array}
    }
};
