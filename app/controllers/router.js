var pages = require('./pages'),
    db_func = require('../db/db'),
    config = require('../../config');

var l_client = global.winston.loggers.get('client'),
    l_log = global.winston.loggers.get('log');

module.exports = function (app) {

    app.get('/', pages.home);

    app.get('/player', pages.player);

    app.get('/join', pages.join);

    app.post('/auth', pages.auth);

    app.post('/logout', pages.logout);

    app.post('/intro', function (req, res) {
        l_client.info(req.session.sweetId, (req.body.state ? 'Passed' : 'Started'), 'intro');
        db_func.skipIntro(req.session.sweetId, (req.body.state || false));
        return res.end();
    });

    app.post(config.git.url, function (req, res) {
        l_log.info('Git pull started!');
        require('../dev/git_controller').git_pull();
        return res.end();
    });

    app.post('/db/playlist', function (req, res) {
        if (req.session.sweetId) {
            db_func.getPlaylist(req.session.sweetId, function (data) {
                if (data === null) {
                    l_client.info(req.session.sweetId, 'No playlist found');
                    return res.end();
                }
                l_client.info(req.session.sweetId, 'Playlist success.');
                res.send(data);
            });
        } else {
            res.end();
        }
    });

    app.post('/db/playlist_add', function (req, res) {
        if (req.session.sweetId) {
            db_func.addToPlaylist(req.session.sweetId, req.body.pl_name, req.body.name, req.body.track, function () {
                l_client.info(req.session.sweetId, 'Added to playlist', req.body.track);
                res.end();
            });
        } else {
            res.end();
        }
    });

    app.post('/db/playlist_remove', function (req, res) {
        if (req.session.sweetId) {
            db_func.removeFromPlaylist(req.session.sweetId, req.body.pl_name, req.body.track, function () {
                l_client.info(req.session.sweetId, 'Removed from playlist', req.body.track);
                res.end();
            });
        } else {
            res.end();
        }
    });

    app.use(pages.page404);
    app.use(pages.page500);
};