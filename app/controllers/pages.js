var db_func = require('../db/db'),
    crypto = require('crypto'),
    config = require('../../config');

var l_client = global.winston.loggers.get('client');

exports.home = function (req, res) {
    res.render("pages/home", {
        title: "ChronoTone"
    })
};

exports.player = function (req, res) {
    if (req.session.sweetId && req.session.sweetyToken) {
        var id = req.session.sweetId,
            admin = false;
        if (id === "44803052" || id === "120616799") admin = true;
        res.render("pages/player", {
            title: "ChronoTone",
            admin: admin
        })
    } else {
        res.redirect('/join');
    }
};

exports.join = function (req, res) {
    if (req.session.sweetId && req.session.sweetyToken) {
        res.redirect('/player');
        return;
    }

    res.render("pages/join");
};

exports.auth = function (req, res) {
    var secretKey = config.vk.clientSecret;

    var sig = req.body.sig,
        expire = req.body.expire,
        mid = req.body.mid,
        secret = req.body.secret,
        sid = req.body.sid,
        user = req.body.user,
        fullname = user.first_name + user.last_name;

    var str = "expire=" + expire + "mid=" + mid + "secret=" + secret + "sid=" + sid + secretKey;
    var hash = crypto.createHash('md5').update(str).digest('hex');

    if(hash == sig){
        if (req.cookies.vk_app_5180223) {
            var cache_sig = req.cookies.vk_app_5180223;
            cache_sig = cache_sig.slice(cache_sig.indexOf("&sig=")+5, cache_sig.length);
            if(sig !== cache_sig) {
                l_client.error('Trying to spoof ID!! Hash:', hash, 'Sig:' , sig, 'Cache sig:', cache_sig);
                return res.end();
            }
        }
        if(req.session.sweetyToken) {
            l_client.info(req.session.sweetId, 'Have Token.');
        } else {
            l_client.info(mid, 'Token Granted');
            db_func.createToken(mid, function (token) {
                req.session.sweetyToken = token;
                req.session.sweetId = mid;
            });
        }
        db_func.login(mid, fullname, function (data) {
            if (data === null) {
                l_client.error('Login failed');
                return res.end();
            }
            l_client.info(req.session.sweetId, 'Login success.');
            res.send(data);
        });
    } else {
        l_client.error('Trying to spoof ID!! Original:', hash, 'Get:' , sig);
        return res.end();
    }

};

exports.logout = function (req, res) {
    if (req.session.sweetyToken) {
        l_client.info(req.session.sweetId, 'Logged out.');
        req.session.destroy();
    }
    res.end();
};

exports.page404 = function (req, res) {
    l_client.error('[ERROR 404] ', req.ip, 'ID:', (req.session.sweetId || 'no ID in session'), 'URL:', req.url);
    res.status(400);
    res.render("pages/error", {
        error: "404",
        message: "Nothing Found"
    })
};

exports.page500 = function (error, req, res) {
    l_client.error('[ERROR 500] ' + req.ip + (' ID: ' + req.session.sweetId || ', no ID in session'));
    res.status(500);
    res.render("pages/error", {
        error: error,
        message: "Internal Server Error"
    })
};