var config = require('../../config'),
    passport = require('passport'),
    AuthLocalStrategy = require('passport-local').Strategy,
    AuthVKStrategy = require('passport-vkontakte').Strategy;

// passport.use('local', new AuthLocalStrategy(
//     function (username, password, done) {
//
//         if (username == "admin" && password == "admin") {
//             return done(null, {
//                 username: "admin",
//                 photoUrl: "url_to_avatar",
//                 profileUrl: "url_to_profile"
//             });
//         }
//
//         return done(null, false, {
//             message: 'Неверный логин или пароль'
//         });
//     }
// ));

passport.use('vk', new AuthVKStrategy({
        clientID: config.vk.clientId,
        clientSecret: config.vk.clientSecret,
        callbackURL: config.vk.callbackURL
    },
    function (accessToken, refreshToken, profile, done) {
        return done(null, {
            id: profile.id,
            username: profile.displayName,
            displayName: profile.displayName,
            profileUrl: profile.profileUrl
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, JSON.stringify(user));
});


passport.deserializeUser(function (data, done) {
    try {
        done(null, JSON.parse(data));
    } catch (e) {
        done(err)
    }
});

module.exports.passport = passport;