global.winston = require('winston');
var config = require('../../config').server;
winston.emitErrs = true;

winston.loggers.add('log', {
    console: {
        level: 'info',
        colorize: true,
        json: false,
        timestamp:true,
        label: 'LOG'
    },
    file: {
        filename: config.log,
        maxsize: 5242880,
        maxFiles: 2,
        colorize: false,
        json: false,
        timestamp:true,
        label: 'LOG'
    }
});

winston.loggers.add('webserver', {
    console: {
        level: 'info',
        colorize: true,
        json: false,
        timestamp:true,
        label: 'WEBSERVER'
    },
    file: {
        filename: config.log,
        maxsize: 5242880,
        maxFiles: 2,
        colorize: false,
        json: false,
        timestamp:true,
        label: 'WEBSERVER'
    }
});

winston.loggers.add('worker', {
    console: {
        level: 'info',
        colorize: true,
        json: false,
        timestamp:true,
        label: 'WORKER'
    },
    file: {
        filename: config.log,
        maxsize: 5242880,
        maxFiles: 2,
        colorize: false,
        json: false,
        timestamp:true,
        label: 'WORKER'
    }
});

winston.loggers.add('proxy', {
    console: {
        level: 'info',
        colorize: true,
        json: false,
        timestamp:true,
        label: 'PROXY'
    },
    file: {
        filename: config.log,
        maxsize: 5242880,
        maxFiles: 2,
        colorize: false,
        json: false,
        timestamp:true,
        label: 'PROXY'
    }
});

winston.loggers.add('client', {
    console: {
        level: 'info',
        colorize: true,
        json: false,
        timestamp:true,
        label: 'CLIENT'
    },
    file: {
        filename: config.log,
        maxsize: 5242880,
        maxFiles: 2,
        colorize: false,
        json: false,
        timestamp:true,
        label: 'CLIENT'
    }
});

winston.loggers.add('db', {
    console: {
        level: 'info',
        colorize: true,
        json: false,
        timestamp:true,
        label: 'DB'
    },
    file: {
        filename: config.log,
        maxsize: 5242880,
        maxFiles: 2,
        colorize: false,
        json: false,
        timestamp:true,
        label: 'DB'
    }
});