var l_log = global.winston.loggers.get('log');

process.stdin.resume();//so the program will not close instantly

function exitHandler(options, err) {
    if (options.restart) {
        global.stop = true;
        l_log.warn('[RESTART]');
        // setTimeout(function () {
            process.kill(process.pid, 'SIGUSR2');
        // }, 2000);
    }
    if (err) console.log(err.stack);
    if (options.exit) {
        global.stop = true;
        l_log.warn('Shutting Down!');
        setTimeout(function () {
            process.exit();
        }, 2000);
    }
}

process.on('exit', exitHandler.bind(null, {exit: true}));

process.on('SIGINT', exitHandler.bind(null, {exit: true}));

// process.on('uncaughtException', exitHandler.bind(null, {restart: true}));

process.once('SIGUSR2', exitHandler.bind(null, {restart: true}));