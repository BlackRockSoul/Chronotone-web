var readline = require('readline'),
    util = require('util');

var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout,
    terminal: false
});

rl.on('line', function(data){
    data = data.split(' ');
    switch (data[0]) {
        case "user_list":
            // console.log(require('../websocket/clients').clientList);
            break;
        default:
            console.log('Unknown command');
            break;
    }
});