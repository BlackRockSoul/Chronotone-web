var config = require('../../config'),
    exec = require('child_process').exec,
    util = require('util'),
    events = require('events'),
    emitter = new events.EventEmitter(),
    l_log = global.winston.loggers.get('log');

emitter
    .on('info', console.info)
    .on('error', console.error)
    .on('severe', function(err) {
        console.error(err);
        process.exit(1);
    });

function puts(error, stdout, stderr) {
    if (error) {
        emitter.emit('error', error);
    } else {
        emitter.emit('info', stdout.trim());
        if (stderr) {
            emitter.emit('error', stderr.trim());
        }
    }
}

module.exports.git_pull = function () {
    l_log.info('git pull %s %s && npm install', config.git.current_branch, config.git.branch);
    exec(util.format('git pull %s %s && npm install', config.git.current_branch, config.git.branch), puts);
};