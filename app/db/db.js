var moment = require('moment'),
    crypto = require('crypto'),
    mongoose = require('mongoose'),
    config = require('../../config'),
    models = require('../models/db_models'),
    db = mongoose.createConnection(config.db.url),
    findOneOrCreate = require('mongoose-find-one-or-create');

var l_db = global.winston.loggers.get('db');

db.on("error", console.error.bind(l_db, "Connection error:"));
db.once("open", function callback() {
    // log(color.blue("[DATABASE]") + "  Connected!");
    mongoose.Promise = global.Promise;
});

var ClientSchema = new mongoose.Schema(models.client);
ClientSchema.plugin(findOneOrCreate);
var Client = db.model('Client', ClientSchema);

var PlaylistSchema = new mongoose.Schema(models.playlist);
PlaylistSchema.plugin(findOneOrCreate);
PlaylistSchema.options.toJSON = {
    transform: function (doc, ret) {
        delete ret._id;
        delete ret.__v;
    }
};
var Playlist = db.model('Playlist', PlaylistSchema);

exports.createToken = function (id, callback) {
    var token,
        salt = "zlq69j41";
    token = id + "bcf84933" + randStr(25) + randInt(0, 10000) + moment() + randStr(10);
    token = token.splice(randInt(15, 50), 0, "BojLclAzcC5vKtm");
    var cipher = crypto.createCipher('aes-256-ctr', salt);
    token = cipher.update(token, 'utf8', 'hex');
    token += cipher.final('hex');
    callback(token);
};

exports.login = function (id, login, callback) {
    if (!id) return callback(null);
    newPartyId(function (partyId) {
        Client.findOneOrCreate({id: id}, {
            id: id,
            partyId: partyId,
            login: login,
            intro: false,
            date: new Date()
        }, function (err, client) {
            if (err) {
                l_db.error(err.message);
                callback(err.message);
            } else {
                callback({
                    id: client.id,
                    partyId: client.partyId,
                    password: client.password,
                    intro: client.intro
                });
            }
        });
    });
};

exports.skipIntro = function (id, state) {
    Client.findOneAndUpdate({id: id}, {$set:{intro: state}}, function (err) {
        if (err) return l_db.error(id, err.message);
    })
};

exports.getPlaylist = function (id, callback) {
    if (!id) return callback(null);
    Playlist.find({id: id}, function (err, playlist) {
        if (err) {
            return l_db.error(id, err.message);
        }
        if (playlist.length) {
            callback(playlist);
        } else {
            callback(null);
        }
    })
};

exports.addToPlaylist = function (id, pl_name, name, track_id, callback) {
    if (!id) return callback(null);
    Playlist.findOne({playlistName: +(id + '.' + pl_name)}, function (err, playlist) {
        if (err) return l_db.error(err.message);
        if (playlist) {
            //Добавить песню в массив и плюсануть количество
            if (playlist.trackList.length) {
                playlist.trackList.push(track_id);
                playlist.amountTracks++;
                playlist.save(function (err) {
                    if (err) return l_db.error(err.message);
                    callback();
                });
            } else {
                //Добавить трек в пустой массив и задать количество
                playlist.trackList = [track_id];
                playlist.amountTracks = 1;
                playlist.save(function (err) {
                    if (err) return l_db.error(err.message);
                    callback();
                });
            }
        } else {
            // Создать плейлист и добавить трек
            Playlist.create({
                id: id,
                playlistName: +(id + '.' + pl_name),
                name: name || ('Чарт ' + pl_name),
                amountTracks: 1,
                trackList: [track_id]
            }, function (err) {
                if (err) return l_db.error(err.message);
                callback();
            });
        }
    })
};

exports.removeFromPlaylist = function (id, pl_name, track_id, callback) {
    if (!id) return callback(null);
    Playlist.findOne({playlistName: +(id + '.' + pl_name)}, function (err, playlist) {
        if (err) {
            l_db.error(err.message);
            return callback(null);
        }
        if (playlist.trackList.indexOf(track_id) >= 0) {
            var pos = playlist.trackList.indexOf(track_id);
            playlist.trackList.splice(pos, 1);
            playlist.amountTracks--;
            playlist.save(function (err) {
                if (err) {
                    l_db.error(err.message);
                    return callback(null);
                }
                callback();
            });
        }
    })
};

function validateToken(token) {
    if (!token) return;
    var dec,
        id,
        salt = "zlq69j41",
        decipher = crypto.createDecipher('aes-256-ctr', salt);
    dec = decipher.update(token, 'hex', 'utf8');
    dec += decipher.final('utf8');
    if (dec.indexOf("BojLclAzcC5vKtm") && dec.indexOf("bcf84933")) {
        id = dec.substring(0, dec.indexOf("bcf84933"));
    } else {
        id = false;
    }
    return id;
}

exports.validateToken = validateToken;

function newPartyId (callback) {
    var partyId = "",
        possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    for (var i = 0; i < 6; i++) {
        partyId += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    Client.findOne({partyId: partyId}, function (err, exist) {
        if (exist) {
            log('PartyId exist: ', partyId, exist);
            partyId = newPartyId();
        } else {
            return callback(partyId);
        }
    });
}

function randStr(len) {
    var text = "",
        possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    for (var i = 0; i < len; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

function randInt(low, high) {
    return Math.floor(Math.random() * (high - low) + low);
}

String.prototype.splice = function (idx, rem, str) {
    return this.slice(0, idx) + str + this.slice(idx + Math.abs(rem));
};