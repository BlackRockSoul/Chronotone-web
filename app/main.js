var express = require('express'),
    app = express(),
    ejsLocals = require('ejs-locals'),
    helmet = require('helmet'),
    Ddos = require('ddos'),
    ddos = new Ddos({
        burst:5,
        limit:60,
        errormessage:'Охладись немного, бро',
        maxexpiry: 30,
        silentStart: true
    }),
    compression = require('compression'),
    session = require('express-session'),
    config = require('./../config'),
    MongoStore = require('connect-mongo')(session),
    SessionStore = new MongoStore(config.db),
    favicon = require('serve-favicon'),
    bodyParser = require('body-parser');

var cookieParser = require('cookie-parser');

app.engine('ejs', ejsLocals);
app.enable('trust proxy');
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(ddos.express);
app.use(helmet());
app.use(compression());
app.use(favicon('public/images/favicon.ico'));
app.use(express.static('public'));
app.use(cookieParser());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(session({
    secret: 'E9TZnseUJNy49B4K92gX59D2L8hTWzB4',
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 24 * 60 * 60 * 1000 //one day
    },
    store: SessionStore
}));

require('./controllers/router')(app);

module.exports = app;
module.exports.SessionStore = SessionStore;